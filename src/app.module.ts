import { Module } from '@nestjs/common';

import { TuitsModule } from './modules/tuits/tuits.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './modules/users/users.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TuitsModule, 
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '192.168.0.12',
      port: 5435,
      username: 'postgres',
      password: 'admin',
      database: 'tuitter',
      autoLoadEntities: true,
      synchronize: true,  
      }),
    UsersModule
  ],
})
export class AppModule {}
