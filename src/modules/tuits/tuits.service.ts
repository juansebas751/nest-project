import { Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { Tuit } from './entities/tuit.entity';
import { CreateTuitDto, UpdateTuitDto } from './dto';
import { User } from '../users/entities';
import { TuitUsers } from './entities';

@Injectable()
export class TuitsService {
    constructor(
    @InjectRepository(Tuit) private readonly tuitRepository: Repository<Tuit>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    // @InjectRepository(TuitUsers) private readonly tuitUsersRepository: Repository<TuitUsers>
    ) {}


    // async getTuitRepository(): Promise<TuitUsers[]> {
    //     return await this.tuitUsersRepository.find()
    // }

    async getTuits(): Promise<Tuit[]> {
        return await this.tuitRepository.find({ relations: ['user'] });
    }

    async getTuit(id: number): Promise<Tuit> {
        const tuit: Tuit = await this.tuitRepository.findOne({ where: { id }, relations: ['user'] });
    
        if (!tuit ) {
            throw new NotFoundException("Resource not found")
        }

        return tuit;
    }

    async createTuit({ message, user }: CreateTuitDto) {
        const tuit: Tuit = this.tuitRepository.create({ message, user });
        return this.tuitRepository.save(tuit);
    }

    async updatedTuit(id: number, { message }: UpdateTuitDto) {
        const tuit: Tuit = await this.tuitRepository.preload({
            id,
            message
        });

        if(!tuit){
            throw new NotFoundException('Resource not found');
        }

        return this.tuitRepository.save(tuit);
    }

    async removeTuit(id: number): Promise<void> {
        const tuit: Tuit = await this.tuitRepository.findOneBy({ id })
        
        if(!tuit) {
            throw new NotFoundException('Resource not found');
        }

        this.tuitRepository.remove(tuit);
    }
}
