import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm/dist';

import { TuitsController } from './tuits.controller';
import { TuitsService } from './tuits.service';
import { Tuit, TuitUsers } from './entities';
import { User } from '../users/entities';

@Module({
    imports: [TypeOrmModule.forFeature([Tuit, User, TuitUsers])],
    controllers: [TuitsController],
    providers: [TuitsService],
})
export class TuitsModule {}
