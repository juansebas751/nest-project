import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "../../users/entities";

@Entity()
export class Tuit {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false })
    message: string;

    @ManyToOne(() => User, (user) => user.tuits, { cascade: true })
    @JoinColumn({ name: 'userId' })
    user: User;
}