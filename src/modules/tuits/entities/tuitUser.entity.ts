import { DataSource, ViewColumn, ViewEntity } from "typeorm";
import { Tuit } from "./tuit.entity";
import { User } from "src/modules/users/entities";

@ViewEntity({
    expression: (dataSource: DataSource) =>
        dataSource
            .createQueryBuilder()
            .select("tuit.id", "id")
            .addSelect("tuit.message", "message")
            .addSelect("user.name", "userName")
            .from(Tuit, "tuit")
            .leftJoin(User, "user", "user.id = tuit.userId"),
})

export class TuitUsers {
    @ViewColumn()
    id: number

    @ViewColumn()
    message: string

    @ViewColumn()
    userName: string
}