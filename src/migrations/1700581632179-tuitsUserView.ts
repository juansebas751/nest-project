import { MigrationInterface, QueryRunner } from "typeorm"
import { View } from "typeorm/schema-builder/view/View"

export class TuitsUserView1700581632179 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createView(
            new View({
                name: 'tuitsUserView',
                expression: 
                `select
                    t.id as tuitId,
                    t.message as tuitMessage,
                    u.id as userId
                    u.name as userName
                from
                    tuit t
                inner join user u on
                    u.id = t.tuitsUser`
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('tuitsUserView')
    }
}
